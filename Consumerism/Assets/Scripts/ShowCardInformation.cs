﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowCardInformation : MonoBehaviour {

    //Script By: Sami Zirak

    public GameObject cardOneInfo;

    public GameObject cardTwoInfo;

    public bool upgradeStationActive;

    //public GameObject cardThreeInfo;

    public int cardID;
    

	void Start ()
    {
        cardID = 1;

        cardOneInfo.SetActive(false);

        cardTwoInfo.SetActive(false);

        upgradeStationActive = false;

       // cardThreeInfo.SetActive(false);
	}
	
	
	void Update ()
    {
	
	}

    public void OnMouseOver()
    {
        if(cardID == 1 && upgradeStationActive == false)
        {
            Debug.Log("Card ID is 1 & Is On Hover");
            cardOneInfo.SetActive(true);
        }

        if (cardID == 2 && upgradeStationActive == false)
        {
            Debug.Log("Card ID is 2 & Is On Hover");
            cardTwoInfo.SetActive(true);
        }

       // if (cardID == 3)
       // {
       //     Debug.Log("Card ID is 3 & On Hover");
       //     cardThreeInfo.SetActive(true);
       // }

    }

    public void OnMouseExit()
    {
        if (cardID == 1 && upgradeStationActive == false)
        {
            Debug.Log("Card ID is 1 & Is On Exit");
            cardOneInfo.SetActive(false);
        }

        if (cardID == 2 && upgradeStationActive == false)
        {
            Debug.Log("Card ID is 2 & Is On Exit");
            cardTwoInfo.SetActive(false);
        }

       // if (cardID == 3)
       // {
       //     Debug.Log("Card ID is 3 & On Exit");
       //     cardThreeInfo.SetActive(false);
       // }
    }
}
