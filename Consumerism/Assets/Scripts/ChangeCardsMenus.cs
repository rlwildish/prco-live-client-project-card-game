﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeCardsMenus : MonoBehaviour {

    //Script Made By: Sami Zirak

    public GameObject cardOneMenu;              //The Menu that will be made active when the edit button for card one is pressed.

    public GameObject cardTwoMenu;              //The Menu that will be made active when the edit button for card two is pressed.

    public GameObject cardThreeMenu;            //The Menu that will be made active when the edit button for card three is pressed.

    public GameObject cardFourMenu;             //The Menu that will be made active when the edit button for card four is pressed.

  //  public GameObject cardFiveMenu;             //The menu that will be made active when the edit button for card five is pressed.


    void Start ()
    {

        cardOneMenu.SetActive(false);           //Makes the menu invisible on start

        cardTwoMenu.SetActive(false);           //Makes the menu invisible on start

        cardThreeMenu.SetActive(false);         //Makes the menu invisible on start

        cardFourMenu.SetActive(false);          //Makes the menu invisible on start

     //   cardFiveMenu.SetActive(false);          //Makes the menu invisible on start

	}
	
    void Update ()
    {
        if(cardOneMenu.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))           //If card menu one is active and the player presses "ESC" it will close the menu
        {
            cardOneMenu.SetActive(false);
        }

        if (cardTwoMenu.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))          //If card menu two is active and the player presses "ESC" it will close the menu
        {
            cardTwoMenu.SetActive(false);
        }

        if (cardThreeMenu.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))        //If card menu three is active and the player presses "ESC" it will close the menu
        {
            cardThreeMenu.SetActive(false);
        }

        if (cardFourMenu.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))         //If card menu four is active and the player presses "ESC" it will close the menu
        {
            cardFourMenu.SetActive(false);
        }

    }

    public void EditCardOne()               //Makes card menu one active for the player to interact with
    {
        if(cardTwoMenu.activeInHierarchy == false && cardThreeMenu.activeInHierarchy == false && cardFourMenu.activeInHierarchy ==false)
        {
            cardOneMenu.SetActive(true);
            Debug.Log("WOWWWW");
        }      
    }

    public void EditCardTwo()               //Makes card menu two active for the player to interact with
    {
        if (cardOneMenu.activeInHierarchy == false && cardThreeMenu.activeInHierarchy == false && cardFourMenu.activeInHierarchy == false)
        {
            cardTwoMenu.SetActive(true);
        }
    }

    public void EditCardThree()             //Makes card menu three active for the player to interact with
    {
        if (cardOneMenu.activeInHierarchy == false && cardTwoMenu.activeInHierarchy == false && cardFourMenu.activeInHierarchy == false)
        {
            cardThreeMenu.SetActive(true);
        }

    }

    public void EditCardFour()              //Makes card menu four active for the player to interact with
    {
        if (cardOneMenu.activeInHierarchy == false && cardTwoMenu.activeInHierarchy == false && cardThreeMenu.activeInHierarchy == false)
        {
            cardFourMenu.SetActive(true);
        }
    }

}
