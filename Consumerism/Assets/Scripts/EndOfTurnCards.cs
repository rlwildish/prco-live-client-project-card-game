﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


    public class EndOfTurnCards : MonoBehaviour {

        public Text displayRandom1;
        public Text displayRandom2;
        public Text displayRandom3;

        public Button nextTurn;

        public GameObject startTurnOverlay;
        public GameObject closeCards;

        public GameObject cardSlot1Harvesting;
        public GameObject cardSlot1TransportToManufacturer;
        public GameObject cardSlot1Manufacturer;
        public GameObject cardSlot1TravelToEurope;
        public GameObject cardSlot1Distribution;
        public GameObject cardSlot2Harvesting;
        public GameObject cardSlot2TransportToManufacturer;
        public GameObject cardSlot2Manufacturer;
        public GameObject cardSlot2TravelToEurope;
        public GameObject cardSlot2Distribution;
        public GameObject cardSlot3Harvesting;
        public GameObject cardSlot3TransportToManufacturer;
        public GameObject cardSlot3Manufacturer;
        public GameObject cardSlot3TravelToEurope;
        public GameObject cardSlot3Distribution;

        private int carbonEmission;
        private int ethics;
        private double money;
        private bool hasChangedHarvesting;
        private bool hasChangedManufacturing;
        private bool tMIsBoat;
        private bool tEIsTruck;
        private bool tEIsPlane;
        private bool dIsTrain;

        //Initialising
        int randomValue = 0;


        string[] cardsList = new string[16];
        string[] filterCardList;
        string[] filteredCardList;
        string[] cardList1;
        string[] cardList2;
        string[] cardList3;

        int[] delayList = new int[16];

        public int card0Delay = 0;
        public int card0Duration = 0;
        public int card1Delay = 0;
        public int card1Duration = 0;
        public int card2Delay = 0;
        public int card2Duration = 0;
        public int card3Delay = 0;
        public int card3Duration = 0;
        public int card4Delay = 0;
        public int card4Duration = 0;
        public int card5Delay = 0;
        public int card5Duration = 0;
        public int card6Delay = 0;
        public int card6Duration = 0;
        public int card7Delay = 0;
        public int card7Duration = 0;
        public int card8Delay = 0;
        public int card8Duration = 0;
        public int card9Delay = 0;
        public int card9Duration = 0;
        public int card10Delay = 0;
        public int card10Duration = 0;
        public int card11Delay = 0;
        public int card11Duration = 0;
        public int card12Delay = 0;
        public int card12Duration = 0;
        public int card13Delay = 0;
        public int card13Duration = 0;
        public int card14Delay = 0;
        public int card14Duration = 0;
        public int card15Delay = 0;
        public int card15Duration = 0;

    // Use this for initialization
    void Start()
        {
            //Card descriptions with Prefix and forward slash before it to allow the program to determine what colour card should the text be displayed on
            cardsList[0] = "H/Drought followed by heavy rain so topsoil is damaged leading to a poor harvest";
            cardsList[1] = "H/Forest fire destroys your picking plant and you need to rebuild";
            cardsList[2] = "H/Moving your Harvesting plant has caused hundreds of workers to lose their jobs. Distraught, they damage your plants and refuse to work";
            cardsList[3] = "TM/Ship hull damaged in suez canal. Ship engine left running causing oil to leak into the sea";
            cardsList[4] = "TM/Storms cause delays for your transportation";
            cardsList[5] = "M/Riots in India cause issues for your manufacturing plant";
            cardsList[6] = "M/Moving your manufacturing plant has caused hundreds of workers to lose their jobs. Distraught, they damage your produce and refuse to work";
            cardsList[7] = "M/The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly";
            cardsList[8] = "TE/Civil unrest in the middle east delays your trucks by several days";
            cardsList[9] = "TE/Several middle eastern governments offer protection for your cargo for a small fee";
            cardsList[10] = "TE/Airline and airport strikes cause delays for your products";
            cardsList[11] = "D/Protestors in Europe boycott your products due to human rights issues";
            cardsList[12] = "D/Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import";
            cardsList[13] = "D/Country wide railroad issues cause products to be delayed several days";
            cardsList[14] = "G/You are given a government grant as a reward for fantastic human rights";
            cardsList[15] = "G/You are given a government grant as a reward for low carbon emmisions";

            delayList[0] = card0Delay;
            delayList[1] = card1Delay;
            delayList[2] = card2Delay;
            delayList[3] = card3Delay;
            delayList[4] = card4Delay;
            delayList[5] = card5Delay;
            delayList[6] = card6Delay;
            delayList[7] = card7Delay;
            delayList[8] = card8Delay;
            delayList[9] = card9Delay;
            delayList[10] = card10Delay;
            delayList[11] = card11Delay;
            delayList[12] = card12Delay;
            delayList[13] = card13Delay;
            delayList[14] = card14Delay;
            delayList[15] = card15Delay;


        //set all card UI elements to inactive so they can't be seen
        cardSlot1Harvesting.SetActive(false);
            cardSlot1TransportToManufacturer.SetActive(false);
            cardSlot1Manufacturer.SetActive(false);
            cardSlot1TravelToEurope.SetActive(false);
            cardSlot1Distribution.SetActive(false);
            cardSlot2Harvesting.SetActive(false);
            cardSlot2TransportToManufacturer.SetActive(false);
            cardSlot2Manufacturer.SetActive(false);
            cardSlot2TravelToEurope.SetActive(false);
            cardSlot2Distribution.SetActive(false);
            cardSlot3Harvesting.SetActive(false);
            cardSlot3TransportToManufacturer.SetActive(false);
            cardSlot3Manufacturer.SetActive(false);
            cardSlot3TravelToEurope.SetActive(false);
            cardSlot3Distribution.SetActive(false);

            displayRandom1.gameObject.SetActive(false);
            displayRandom2.gameObject.SetActive(false);
            displayRandom3.gameObject.SetActive(false);

            closeCards.SetActive(false);
            startTurnOverlay.SetActive(false);
        }

        public void GetCards()
        {


            GameObject findResourceLevels = GameObject.Find("Player Manager");

            PlayerResources findResources = findResourceLevels.GetComponent<PlayerResources>();

            money = findResources.GetMoneyLevels();

            ethics = findResources.GetEthicsLevels();

            carbonEmission = findResources.GetCOTwoLevels();

            Debug.Log(money);

            Debug.Log(ethics);

            Debug.Log(carbonEmission);

            //Set card text displays active
            displayRandom1.gameObject.SetActive(true);
            displayRandom2.gameObject.SetActive(true);
            displayRandom3.gameObject.SetActive(true);



            filterCardList = new string[1000];

            for (int i = 0; i < cardsList.Length; i++)
            {
                filterCardList[i] = cardsList[i];
            }

            if (hasChangedHarvesting == false)
            {
                filterCardList[2] = null;
            }

            if (tMIsBoat == false)
            {
                filterCardList[3] = null;
            }

            if (hasChangedManufacturing == false)
            {
                filterCardList[6] = null;
            }

            if (ethics < 25)
            {
                filterCardList[7] = null;
            }

            if (tEIsTruck == false)
            {
                filterCardList[8] = null;
            }

            if (tEIsPlane)
            {
                filterCardList[10] = null;
            }

            if (ethics > -1)
            {
                filterCardList[11] = null;
            }

            if (dIsTrain == false)
            {
                filterCardList[13] = null;
            }

            if (ethics < 50)
            {
                filterCardList[14] = null;
            }

            if (carbonEmission < 50)
            {
                filterCardList[15] = null;
            }

            int filteredCount = 0;

            for (int i = 0; i < filterCardList.Length; i++)
            {

                if (filterCardList[i] == null)
                {
                    filteredCount++;
                }

            }

            int used1;
            int used2;

            cardList1 = new string[filterCardList.Length];
            cardList2 = new string[filterCardList.Length];
            cardList3 = new string[filterCardList.Length];

            string[] cardOne;
            string[] cardTwo;
            string[] cardThree;
            char[] separator = new char[] { '/' };

            //setting cardList1, 2 and 3 to equal cardsList
            for (int i = 0; i < filterCardList.Length; i++)
            {
                cardList1[i] = filterCardList[i];
                cardList2[i] = filterCardList[i];
                cardList3[i] = filterCardList[i];
            }
            //get a random integer value as low as 0 or as high as 15
            randomValue = Random.Range(0, filterCardList.Length);

            if (filterCardList[randomValue] == null || delayList[randomValue] > 0)
            {

                for (int i = 0; i < 1000; i++)
                {
                    randomValue = Random.Range(0, filterCardList.Length);

                    if (filterCardList[randomValue] != null && delayList[randomValue] < 1)
                    {
                        break;
                    }
                }

            }

            //cardOne is set to cardList1[randomValue] but split by the separator (this case is /) cardOne[0] will equal whats before a / and cardOne[1] will equal what is after it
            cardOne = filterCardList[randomValue].Split(separator);
            //the first text element is set to display cardOne[1] the card description
            displayRandom1.text = cardOne[1];

            //finds which card to activate for slot 1
            if (cardOne[0] == "H")
            {
                cardSlot1Harvesting.SetActive(true);
            }
            else if (cardOne[0] == "TM")
            {
                cardSlot1TransportToManufacturer.SetActive(true);
            }
            else if (cardOne[0] == "M")
            {
                cardSlot1Manufacturer.SetActive(true);
            }
            else if (cardOne[0] == "TE")
            {
                cardSlot1TravelToEurope.SetActive(true);
            }
            else if (cardOne[0] == "D")
            {
                cardSlot1Distribution.SetActive(true);
            }
            else if (cardOne[0] == "G")
            {
                cardSlot1TransportToManufacturer.SetActive(true);
            }
            else
            {

            }
            //set used1 to randomValue for future reference
            used1 = randomValue;
            //set cardList2 equal to cardsList
            //for (int i = 0; i < 16; i++)
            //{
            //    cardList2[i] = cardsList[i];
            //}
            //randomValue now set to a new random number
            randomValue = Random.Range(0, filterCardList.Length);
            //if randomValue is equal to used1 then keep getting a random value for the variable until it is different/not equal to used1
            if (randomValue == used1 || filterCardList[randomValue] == null || delayList[randomValue] > 0)
            {
                for (int i = 0; i < 1000; i++)
                {
                    randomValue = Random.Range(0, filterCardList.Length);

                    if (randomValue != used1 && filterCardList[randomValue] != null && delayList[randomValue] < 1)
                    {
                        break;
                    }
                }
            }

            //get two indexes for cardTwo one which knows the card colour and the other that knows the card description
            cardTwo = filterCardList[randomValue].Split(separator);
            //display text 2 displays the card description for card 2
            displayRandom2.text = cardTwo[1];
            //uses cardTwo[0] to find the colour card we need for the description
            if (cardTwo[0] == "H")
            {
                cardSlot2Harvesting.SetActive(true);
            }
            else if (cardTwo[0] == "TM")
            {
                cardSlot2TransportToManufacturer.SetActive(true);
            }
            else if (cardTwo[0] == "M")
            {
                cardSlot2Manufacturer.SetActive(true);
            }
            else if (cardTwo[0] == "TE")
            {
                cardSlot2TravelToEurope.SetActive(true);
            }
            else if (cardTwo[0] == "D")
            {
                cardSlot2Distribution.SetActive(true);
            }
            else if (cardTwo[0] == "G")
            {
                cardSlot2TransportToManufacturer.SetActive(true);
            }
            else
            {

            }
            //used 2 set to randomValue for future reference
            used2 = randomValue;

            //for (int i = 0; i < 16; i++)
            //{
            //    cardList3[i] = cardsList[i];
            //}
            //randomValue changed again for a new random integer value
            randomValue = Random.Range(0, filterCardList.Length);
            //if randomValue is equal to used1 then keep getting a random value for the variable until it is different/not equal to used1 and used 2
            if (randomValue == used1 || randomValue == used2 || filterCardList[randomValue] == null || delayList[randomValue] > 0)
            {
                for (int i = 0; i < 1000; i++)
                {
                    randomValue = Random.Range(0, filterCardList.Length);

                    if (randomValue != used1 && randomValue != used2 && filterCardList[randomValue] != null && delayList[randomValue] < 1)
                    {
                        break;
                    }
                }
            }
            //cardThree is set to the split of cardList3[randomValue]
            cardThree = filterCardList[randomValue].Split(separator);
            //display text 3 is set to the card description for card three
            displayRandom3.text = cardThree[1];
            //uses cardThree[0] to find the coloured card we need
            if (cardThree[0] == "H")
            {
                cardSlot3Harvesting.SetActive(true);
            }
            else if (cardThree[0] == "TM")
            {
                cardSlot3TransportToManufacturer.SetActive(true);
            }
            else if (cardThree[0] == "M")
            {
                cardSlot3Manufacturer.SetActive(true);
            }
            else if (cardThree[0] == "TE")
            {
                cardSlot3TravelToEurope.SetActive(true);
            }
            else if (cardThree[0] == "D")
            {
                cardSlot3Distribution.SetActive(true);
            }
            else if (cardThree[0] == "G")
            {
                cardSlot3TransportToManufacturer.SetActive(true);
            }
            else
            {

            }
            //set the arrays to null so that they carry no data over to the next time the cards are needed
            for (int i = 0; i < filterCardList.Length; i++)
            {
                cardList1[i] = null;
                cardList2[i] = null;
                cardList3[i] = null;
                filterCardList[i] = null;
            }
            //set UI elements active
            startTurnOverlay.SetActive(true);
            closeCards.SetActive(true);

        if (cardOne[1] == "Drought followed by heavy rain so topsoil is damaged leading to a poor harvest" || cardTwo[1] == "Drought followed by heavy rain so topsoil is damaged leading to a poor harvest" || cardThree[1] == "Drought followed by heavy rain so topsoil is damaged leading to a poor harvest")
        {

            GameObject findHarvestingMoney = GameObject.Find("Player Manager");

            PlayerResources findHarvesting = findHarvestingMoney.GetComponent<PlayerResources>();

            findHarvesting.SetHarvestingMoney(-10000);

            findHarvesting.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Forest fire destroys your picking plant and you need to rebuild" || cardTwo[1] == "Forest fire destroys your picking plant and you need to rebuild" || cardThree[1] == "Forest fire destroys your picking plant and you need to rebuild")
        {

            GameObject findHarvestingMoney = GameObject.Find("Player Manager");

            PlayerResources findHarvesting = findHarvestingMoney.GetComponent<PlayerResources>();

            findHarvesting.SetHarvestingMoney(-20000);

            findHarvesting.AlterExpectedIncome(-20000);

            card1Delay = 3;

        }

        if (cardOne[1] == "Moving your Harvesting plant has caused hundreds of workers to lose their jobs. Distraught, they damage your plants and refuse to work" || cardTwo[1] == "Moving your Harvesting plant has caused hundreds of workers to lose their jobs. Distraught, they damage your plants and refuse to work" || cardThree[1] == "Moving your Harvesting plant has caused hundreds of workers to lose their jobs. Distraught, they damage your plants and refuse to work")
        {

            GameObject findHarvestingMoney = GameObject.Find("Player Manager");

            PlayerResources findHarvesting = findHarvestingMoney.GetComponent<PlayerResources>();

            findHarvesting.SetHarvestingMoney(-15000);

            findHarvesting.AlterEthicsLevels(-10);

            findHarvesting.AlterExpectedIncome(-15000);

        }

        if (cardOne[1] == "Ship hull damaged in suez canal. Ship engine left running causing oil to leak into the sea" || cardTwo[1] == "Ship hull damaged in suez canal. Ship engine left running causing oil to leak into the sea" || cardThree[1] == "Ship hull damaged in suez canal. Ship engine left running causing oil to leak into the sea")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetTransToManuMoney(-10000);

            findTransToManu.AlterEthicsLevels(-10);

            findTransToManu.AlterCOTwoLevels(-20);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Storms cause delays for your transportation" || cardTwo[1] == "Storms cause delays for your transportation" || cardThree[1] == "Storms cause delays for your transportation")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetTransToManuMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Riots in India cause issues for your manufacturing plant" || cardTwo[1] == "Riots in India cause issues for your manufacturing plant" || cardThree[1] == "Riots in India cause issues for your manufacturing plant")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetManufacturerMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Moving your manufacturing plant has caused hundreds of workers to lose their jobs. Distraught, they damage your produce and refuse to work" || cardTwo[1] == "Moving your manufacturing plant has caused hundreds of workers to lose their jobs. Distraught, they damage your produce and refuse to work" || cardThree[1] == "Moving your manufacturing plant has caused hundreds of workers to lose their jobs. Distraught, they damage your produce and refuse to work")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetManufacturerMoney(-25000);

            findTransToManu.AlterEthicsLevels(-10);

            findTransToManu.AlterExpectedIncome(-25000);

        }

        if (cardOne[1] == "The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly" || cardTwo[1] == "The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly" || cardThree[1] == "The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly" || card7Duration > 0)
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetManufacturerMoney(5000);

            findTransToManu.AlterEthicsLevels(2);

            findTransToManu.AlterExpectedIncome(5000);

            card7Delay = 100;
            if (cardOne[1] == "The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly" || cardTwo[1] == "The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly" || cardThree[1] == "The local government are pleased with the oppurtunities and revenue you are bringing into their country. As long as you keep your plant here, your human rights will increase slightly")
                card7Duration = 100;

        }

        if (cardOne[1] == "Civil unrest in the middle east delays your trucks by several days" || cardTwo[1] == "Civil unrest in the middle east delays your trucks by several days" || cardThree[1] == "Civil unrest in the middle east delays your trucks by several days")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetTransToDistMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Several middle eastern governments offer protection for your cargo for a small fee" || cardTwo[1] == "Several middle eastern governments offer protection for your cargo for a small fee" || cardThree[1] == "Several middle eastern governments offer protection for your cargo for a small fee")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetTransToDistMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Airline and airport strikes cause delays for your products" || cardTwo[1] == "Airline and airport strikes cause delays for your products" || cardThree[1] == "Airline and airport strikes cause delays for your products")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetTransToDistMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Protestors in Europe boycott your products due to human rights issues" || cardTwo[1] == "Protestors in Europe boycott your products due to human rights issues" || cardThree[1] == "Protestors in Europe boycott your products due to human rights issues")
        {
            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetDistMoney(-10000);

            findTransToManu.AlterEthicsLevels(-20);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import" || cardTwo[1] == "Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import" || cardThree[1] == "Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import" || card12Duration > 0)
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetDistMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

            card12Duration = 10000;
            if (cardOne[1] == "Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import" || cardTwo[1] == "Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import" || cardThree[1] == "Due to new legislation import tax is increased in several countries, landing you with a much higher cost to import")
                card12Delay = 1000000;

        }

        if (cardOne[1] == "Country wide railroad issues cause products to be delayed several days" || cardTwo[1] == "Country wide railroad issues cause products to be delayed several days" || cardThree[1] == "Country wide railroad issues cause products to be delayed several days")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetDistMoney(-10000);

            findTransToManu.AlterExpectedIncome(-10000);

        }

        if (cardOne[1] == "You are given a government grant as a reward for fantastic human rights" || cardTwo[1] == "You are given a government grant as a reward for fantastic human rights" || cardThree[1] == "You are given a government grant as a reward for fantastic human rights")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetDistMoney(10000);

            findTransToManu.AlterEthicsLevels(20);

            findTransToManu.AlterExpectedIncome(10000);

            card14Delay = 3;

        }

        if (cardOne[1] == "You are given a government grant as a reward for low carbon emmisions" || cardTwo[1] == "You are given a government grant as a reward for low carbon emmisions" || cardThree[1] == "You are given a government grant as a reward for low carbon emmisions")
        {

            GameObject findTransToManuMoney = GameObject.Find("Player Manager");

            PlayerResources findTransToManu = findTransToManuMoney.GetComponent<PlayerResources>();

            findTransToManu.SetDistMoney(10000);

            findTransToManu.AlterEthicsLevels(20);

            findTransToManu.AlterExpectedIncome(10000);

            card15Delay = 3;

        }




    }

        public void CloseCards()
        {
            //set all of the UI elements for the cards inactive
            cardSlot1Harvesting.SetActive(false);
            cardSlot1TransportToManufacturer.SetActive(false);
            cardSlot1Manufacturer.SetActive(false);
            cardSlot1TravelToEurope.SetActive(false);
            cardSlot1Distribution.SetActive(false);
            cardSlot2Harvesting.SetActive(false);
            cardSlot2TransportToManufacturer.SetActive(false);
            cardSlot2Manufacturer.SetActive(false);
            cardSlot2TravelToEurope.SetActive(false);
            cardSlot2Distribution.SetActive(false);
            cardSlot3Harvesting.SetActive(false);
            cardSlot3TransportToManufacturer.SetActive(false);
            cardSlot3Manufacturer.SetActive(false);
            cardSlot3TravelToEurope.SetActive(false);
            cardSlot3Distribution.SetActive(false);

            closeCards.SetActive(false);
            startTurnOverlay.SetActive(false);

            displayRandom1.text = "";
            displayRandom2.text = "";
            displayRandom3.text = "";

            displayRandom1.gameObject.SetActive(false);
            displayRandom2.gameObject.SetActive(false);
            displayRandom3.gameObject.SetActive(false);

        }

        public void SetCarbonEmission(int carbonEmissions)
        {

            carbonEmission = carbonEmissions;

        }

        public void SetEthics(int ethic)
        {

            ethics = ethic;

        }

        public void SetHasChangedHarvesting(bool changedHarvesting)
        {

            hasChangedHarvesting = changedHarvesting;

        }

        public void SetHasChangedManufacturer(bool changedManufacturer)
        {

            hasChangedManufacturing = changedManufacturer;

        }

        public void SetTMIsBoat()
        {

        }

        public void SetTEIsTruck()
        {

        }

        public void SetTEIsPlane()
        {

        }

        public void SetDIsTrain()
        {

        }
    }

