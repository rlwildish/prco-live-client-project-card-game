﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NextTurnCalculate : MonoBehaviour {

    //Script By: Sami Zirak

    [SerializeField]
    private double moneySpentThisTurn;

    [SerializeField]
    private double moneyMadeThisTurn;

    [SerializeField]
    private double moneyMultiplier;

    public double estimatedIncome;

    public Text expectedIncomeThisTurn;

	void Start ()
    {
        moneySpentThisTurn = 0.0;

        moneyMadeThisTurn = 0.0;

        moneyMultiplier = 1.0;

        estimatedIncome = 0.0;
	}
	
	void Update ()
    {
        //estimatedIncome = (moneyMadeThisTurn * moneyMultiplier) - moneySpentThisTurn;

        //expectedIncomeThisTurn.text = "£" + estimatedIncome.ToString();
	}

    public void CalculateEarnings()
    {
        GameObject playerManager = GameObject.Find("Player Manager");

        PlayerResources playersMoney = playerManager.GetComponent<PlayerResources>();

        playersMoney.moneyLevels = playersMoney.moneyLevels + moneyMadeThisTurn * moneyMultiplier;

        playersMoney.moneyLevels = playersMoney.moneyLevels - moneySpentThisTurn;

        ResetValuesForNewTurn();
    }

    public void ResetValuesForNewTurn()
    {
        moneySpentThisTurn = 0.0;

        moneyMadeThisTurn = 0.0;

        moneyMultiplier = 1.0;

        estimatedIncome = 0.0;
    }
}
