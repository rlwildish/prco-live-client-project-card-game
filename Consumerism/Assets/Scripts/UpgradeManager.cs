﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour {

    //Script By: Sami Zirak

    public Button harvestingUpgradeOne;

    public Button harvestingUpgradeTwo;

    public Button harvestingButton;

    public Button transportToManButton;

    public Button manufactureButton;

    public Button transportToDisButton;

    public Button distributionButton;

    public Button nextTurnButton;

    public Text currentTurnText;

    public CanvasGroup harvestingButtonCanvasGroup;

    public CanvasGroup transOneButtonCanvasGroup;

    public CanvasGroup manufacturingButtonCanvasGroup;

    public CanvasGroup transTwoButtonCanvasGroup;

    public CanvasGroup europeDistributionButtonCanvasGroup;

    public CanvasGroup harvestingCanvasGroup;

    public CanvasGroup transportOneCanvasGroup;

    public CanvasGroup manufacturingCanvasGroup;

    public CanvasGroup transportTwoCanvasGroup;

    public CanvasGroup europeDistributionCanvasGroup;

    public CanvasGroup mainUICanvasGroup;

    public int clickCount = 1;

    public bool inAction;

    void Start()
    {

        clickCount = 1;

        harvestingCanvasGroup.alpha = 0;

        harvestingCanvasGroup.interactable = false;

        harvestingCanvasGroup.blocksRaycasts = false;

        transportOneCanvasGroup.alpha = 0;

        transportOneCanvasGroup.interactable = false;

        transportOneCanvasGroup.blocksRaycasts = false;

        manufacturingCanvasGroup.alpha = 0;

        manufacturingCanvasGroup.interactable = false;

        manufacturingCanvasGroup.blocksRaycasts = false;

        transportTwoCanvasGroup.alpha = 0;

        transportTwoCanvasGroup.interactable = false;

        transportTwoCanvasGroup.blocksRaycasts = false;

        europeDistributionCanvasGroup.alpha = 0;

        europeDistributionCanvasGroup.interactable = false;

        europeDistributionCanvasGroup.blocksRaycasts = false;

        inAction = false;

    }

    IEnumerator DoFadeInMainCanvas()
    {

        inAction = true;

        while (mainUICanvasGroup.alpha < 0.999F)
        {
            mainUICanvasGroup.alpha += Time.deltaTime / 0.3F;

            yield return null;
        }

        mainUICanvasGroup.blocksRaycasts = true;

        mainUICanvasGroup.interactable = true;

        yield return null;

        inAction = false;
    }


    IEnumerator DoFadeInHarvestingUpgrades()
    {
        inAction = true;

        while (harvestingCanvasGroup.alpha < 0.999F && transOneButtonCanvasGroup.alpha > 0 && manufacturingButtonCanvasGroup.alpha > 0 && transTwoButtonCanvasGroup.alpha > 0 && europeDistributionButtonCanvasGroup.alpha > 0)
        {
            harvestingCanvasGroup.alpha += Time.deltaTime / 0.3F;

            transOneButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            yield return null;
        }

        harvestingCanvasGroup.blocksRaycasts = true;

        harvestingCanvasGroup.interactable = true;

        transOneButtonCanvasGroup.blocksRaycasts = false;

        transOneButtonCanvasGroup.interactable = false;

        manufacturingButtonCanvasGroup.blocksRaycasts = false;

        manufacturingButtonCanvasGroup.interactable = false;

        transTwoButtonCanvasGroup.blocksRaycasts = false;

        transTwoButtonCanvasGroup.interactable = false;

        europeDistributionButtonCanvasGroup.blocksRaycasts = false;

        europeDistributionButtonCanvasGroup.interactable = false;

        yield return null;

        inAction = false;
    }


    IEnumerator DoFadeOutMainCanvas()
    {
        inAction = true;

        while (mainUICanvasGroup.alpha > 0)
        {
            mainUICanvasGroup.alpha -= Time.deltaTime / 0.3F;

            yield return null;
        }

        mainUICanvasGroup.blocksRaycasts = false;

        mainUICanvasGroup.interactable = false;

        yield return null;

        inAction = false;
    }


    IEnumerator DoFadeOutHarvestingUpgrades()
    {
        inAction = true;

        while (harvestingCanvasGroup.alpha > 0 && transOneButtonCanvasGroup.alpha < 0.999F && manufacturingButtonCanvasGroup.alpha < 0.999F && transTwoButtonCanvasGroup.alpha < 0.999F && europeDistributionButtonCanvasGroup.alpha < 0.999F)
        {
            harvestingCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            transOneButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            yield return null;
        }

        harvestingCanvasGroup.blocksRaycasts = false;

        harvestingCanvasGroup.interactable = false;

        transOneButtonCanvasGroup.blocksRaycasts = true;

        transOneButtonCanvasGroup.interactable = true;

        manufacturingButtonCanvasGroup.blocksRaycasts = true;

        manufacturingButtonCanvasGroup.interactable = true;

        transTwoButtonCanvasGroup.blocksRaycasts = true;

        transTwoButtonCanvasGroup.interactable = true;

        europeDistributionButtonCanvasGroup.blocksRaycasts = true;

        europeDistributionButtonCanvasGroup.interactable = true;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeInTransportOneUpgrades()
    {
        inAction = true;

        while (transportOneCanvasGroup.alpha < 0.999F && harvestingButtonCanvasGroup.alpha > 0 && manufacturingButtonCanvasGroup.alpha > 0 && transTwoButtonCanvasGroup.alpha > 0 && europeDistributionButtonCanvasGroup.alpha > 0)
        {
            transportOneCanvasGroup.alpha += Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            yield return null;
        }

        transportOneCanvasGroup.blocksRaycasts = true;

        transportOneCanvasGroup.interactable = true;

        harvestingButtonCanvasGroup.blocksRaycasts = false;

        harvestingButtonCanvasGroup.interactable = false;

        manufacturingButtonCanvasGroup.blocksRaycasts = false;

        manufacturingButtonCanvasGroup.interactable = false;

        transTwoButtonCanvasGroup.blocksRaycasts = false;

        transTwoButtonCanvasGroup.interactable = false;

        europeDistributionButtonCanvasGroup.blocksRaycasts = false;

        europeDistributionButtonCanvasGroup.interactable = false;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeOutTransportOneUpgrades()
    {

        inAction = true;

        while (transportOneCanvasGroup.alpha > 0 && harvestingButtonCanvasGroup.alpha < 0.999F && manufacturingButtonCanvasGroup.alpha < 0.999F && transTwoButtonCanvasGroup.alpha < 0.999F && europeDistributionButtonCanvasGroup.alpha <0.999F)
        {
            transportOneCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            yield return null;
        }

        transportOneCanvasGroup.blocksRaycasts = false;

        transportOneCanvasGroup.interactable = false;

        harvestingButtonCanvasGroup.blocksRaycasts = true;

        harvestingButtonCanvasGroup.interactable = true;

        manufacturingButtonCanvasGroup.blocksRaycasts = true;

        manufacturingButtonCanvasGroup.interactable = true;

        transTwoButtonCanvasGroup.blocksRaycasts = true;

        transTwoButtonCanvasGroup.interactable = true;

        europeDistributionButtonCanvasGroup.blocksRaycasts = true;

        europeDistributionButtonCanvasGroup.interactable = true;

        yield return null;

        inAction = false;

    }

    IEnumerator DoFadeInManufacturingUpgrades()
    {

        inAction = true;

        while (transOneButtonCanvasGroup.alpha > 0 && harvestingButtonCanvasGroup.alpha > 0 && manufacturingCanvasGroup.alpha < 0.999F && transTwoButtonCanvasGroup.alpha > 0 && europeDistributionButtonCanvasGroup.alpha > 0)
        {
            transOneButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            manufacturingCanvasGroup.alpha += Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            yield return null;
        }

        transOneButtonCanvasGroup.blocksRaycasts = false;

        transportOneCanvasGroup.interactable = false;

        harvestingButtonCanvasGroup.blocksRaycasts = false;

        harvestingButtonCanvasGroup.interactable = false;

        manufacturingCanvasGroup.blocksRaycasts = true;

        manufacturingCanvasGroup.interactable = true;

        transTwoButtonCanvasGroup.blocksRaycasts = false;

        transTwoButtonCanvasGroup.interactable = false;

        europeDistributionButtonCanvasGroup.blocksRaycasts = false;

        europeDistributionButtonCanvasGroup.interactable = false;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeOutManufacturingUpgrades()
    {

        inAction = true;

        while (transOneButtonCanvasGroup.alpha < 0.999F && harvestingButtonCanvasGroup.alpha < 0.999F && manufacturingCanvasGroup.alpha > 0 && transTwoButtonCanvasGroup.alpha < 0.999F && europeDistributionButtonCanvasGroup.alpha < 0.999F)
        {
            transOneButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            manufacturingCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            yield return null;
        }

        transOneButtonCanvasGroup.blocksRaycasts = true;

        transOneButtonCanvasGroup.interactable = true;

        harvestingButtonCanvasGroup.blocksRaycasts = true;

        harvestingButtonCanvasGroup.interactable = true;

        manufacturingCanvasGroup.blocksRaycasts = false;

        manufacturingCanvasGroup.interactable = false;

        transTwoButtonCanvasGroup.blocksRaycasts = true;

        transTwoButtonCanvasGroup.interactable = true;

        europeDistributionButtonCanvasGroup.blocksRaycasts = true;

        europeDistributionButtonCanvasGroup.interactable = true;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeInTransportTwoUpgrades()
    {

        inAction = true;

        while (transOneButtonCanvasGroup.alpha > 0 && harvestingButtonCanvasGroup.alpha > 0 && manufacturingButtonCanvasGroup.alpha > 0 && transportTwoCanvasGroup.alpha < 0.999F && europeDistributionButtonCanvasGroup.alpha > 0)
        {
            transOneButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            transportTwoCanvasGroup.alpha += Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            yield return null;
        }

        transOneButtonCanvasGroup.blocksRaycasts = false;

        transportOneCanvasGroup.interactable = false;

        harvestingButtonCanvasGroup.blocksRaycasts = false;

        harvestingButtonCanvasGroup.interactable = false;

        manufacturingButtonCanvasGroup.blocksRaycasts = false;

        manufacturingButtonCanvasGroup.interactable = false;

        transportTwoCanvasGroup.blocksRaycasts = true;

        transportTwoCanvasGroup.interactable = true;

        europeDistributionButtonCanvasGroup.blocksRaycasts = false;

        europeDistributionButtonCanvasGroup.interactable = false;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeOutTransportTwoUpgrades()
    {

        inAction = true;

        while (transOneButtonCanvasGroup.alpha < 0.999F && harvestingButtonCanvasGroup.alpha < 0.999F && manufacturingButtonCanvasGroup.alpha < 0.999F && transportTwoCanvasGroup.alpha > 0 && europeDistributionButtonCanvasGroup.alpha < 0.999F)
        {
            transOneButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            transportTwoCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            europeDistributionButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            yield return null;
        }

        transOneButtonCanvasGroup.blocksRaycasts = true;

        transOneButtonCanvasGroup.interactable = true;

        harvestingButtonCanvasGroup.blocksRaycasts = true;

        harvestingButtonCanvasGroup.interactable = true;

        manufacturingButtonCanvasGroup.blocksRaycasts = true;

        manufacturingButtonCanvasGroup.interactable = true;

        transportTwoCanvasGroup.blocksRaycasts = false;

        transportTwoCanvasGroup.interactable = false;

        europeDistributionButtonCanvasGroup.blocksRaycasts = true;

        europeDistributionButtonCanvasGroup.interactable = true;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeInEuropeDisUpgrades()
    {
        inAction = true;

        while (transOneButtonCanvasGroup.alpha > 0 && harvestingButtonCanvasGroup.alpha > 0 && manufacturingButtonCanvasGroup.alpha > 0 && transTwoButtonCanvasGroup.alpha > 0 && europeDistributionCanvasGroup.alpha < 0.999F)
        {
            transOneButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            europeDistributionCanvasGroup.alpha += Time.deltaTime / 0.3F;

            yield return null;
        }

        transOneButtonCanvasGroup.blocksRaycasts = false;

        transportOneCanvasGroup.interactable = false;

        harvestingButtonCanvasGroup.blocksRaycasts = false;

        harvestingButtonCanvasGroup.interactable = false;

        manufacturingButtonCanvasGroup.blocksRaycasts = false;

        manufacturingButtonCanvasGroup.interactable = false;

        transTwoButtonCanvasGroup.blocksRaycasts = false;

        transTwoButtonCanvasGroup.interactable = false;

        europeDistributionCanvasGroup.blocksRaycasts = true;

        europeDistributionCanvasGroup.interactable = true;

        yield return null;

        inAction = false;
    }

    IEnumerator DoFadeOutEuropeDisUpgrades()
    {
        inAction = true;

        while (transOneButtonCanvasGroup.alpha < 0.999F && harvestingButtonCanvasGroup.alpha < 0.999F && manufacturingButtonCanvasGroup.alpha < 0.999F && transTwoButtonCanvasGroup.alpha < 0.999F && europeDistributionCanvasGroup.alpha > 0)
        {
            transOneButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            harvestingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            manufacturingButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            transTwoButtonCanvasGroup.alpha += Time.deltaTime / 0.3F;

            europeDistributionCanvasGroup.alpha -= Time.deltaTime / 0.3F;

            yield return null;
        }

        transOneButtonCanvasGroup.blocksRaycasts = true;

        transOneButtonCanvasGroup.interactable = true;

        harvestingButtonCanvasGroup.blocksRaycasts = true;

        harvestingButtonCanvasGroup.interactable = true;

        manufacturingButtonCanvasGroup.blocksRaycasts = true;

        manufacturingButtonCanvasGroup.interactable = true;

        transTwoButtonCanvasGroup.blocksRaycasts = true;

        transTwoButtonCanvasGroup.interactable = true;

        europeDistributionCanvasGroup.blocksRaycasts = false;

        europeDistributionCanvasGroup.interactable = false;

        yield return null;

        inAction = false;

    }

    public void FadeInEuropeDisUpgrades()
    {
        StartCoroutine(DoFadeInEuropeDisUpgrades());

        StartCoroutine(DoFadeOutMainCanvas());
    }

    public void FadeOutEuropeDisUpgrades()
    {
        StartCoroutine(DoFadeOutEuropeDisUpgrades());

        StartCoroutine(DoFadeInMainCanvas());
    }

    public void FadeInTranTwoUpgrades()
    {
        StartCoroutine(DoFadeInTransportTwoUpgrades());

        StartCoroutine(DoFadeOutMainCanvas());
    }

    public void FadeOutTranTwoUpgrades()
    {
        StartCoroutine(DoFadeOutTransportTwoUpgrades());

        StartCoroutine(DoFadeInMainCanvas());
    }

    public void FadeInManUpgrades()
    {
        StartCoroutine(DoFadeInManufacturingUpgrades());

        StartCoroutine(DoFadeOutMainCanvas());
    }

    public void FadeOutManUpgrades()
    {
        StartCoroutine(DoFadeOutManufacturingUpgrades());

        StartCoroutine(DoFadeInMainCanvas());
    }

    public void FadeInTranOneUpgrades()
    {
        StartCoroutine(DoFadeInTransportOneUpgrades());

        StartCoroutine(DoFadeOutMainCanvas());
    }

    public void FadeOutTranOneUpgrades()
    {
        StartCoroutine(DoFadeOutTransportOneUpgrades());

        StartCoroutine(DoFadeInMainCanvas());
    }

    public void FadeOutHarvestingUpgrades()
    {
        StartCoroutine(DoFadeOutHarvestingUpgrades());

        StartCoroutine(DoFadeInMainCanvas());
    }

    public void FadeInHarvestingUpgrades()
    {
        StartCoroutine(DoFadeInHarvestingUpgrades());

        StartCoroutine(DoFadeOutMainCanvas());
    }


    public void HarvestingButtonClicked()
    {
        if (inAction == false)
        {
            clickCount = clickCount - 1;

            GameObject harvestButtonInGame = GameObject.Find("Harvesting Button");

            ShowCardInformation checkForUpgradeActivity = harvestButtonInGame.GetComponent<ShowCardInformation>();


            if (clickCount == 0)
            {
                checkForUpgradeActivity.upgradeStationActive = true;

                if (checkForUpgradeActivity.cardID == 1)
                {
                    checkForUpgradeActivity.cardOneInfo.SetActive(false);
                    
                }

                if (checkForUpgradeActivity.cardID == 2)
                {
                    checkForUpgradeActivity.cardTwoInfo.SetActive(false);
                }

                FadeInHarvestingUpgrades();

                clickCount = 2;
            }

            if (clickCount == 1)
            {
                checkForUpgradeActivity.upgradeStationActive = false;

                FadeOutHarvestingUpgrades();

                clickCount = 1;
            }
        }
    }

    public void TransportToManButtonClicked()
    {
        if(inAction == false)
        {
            clickCount = clickCount - 1;

            GameObject transportToManButtonInGame = GameObject.Find("Transportation To Manuf Button");

            ShowCardInformation checkForUpgradeActivity = transportToManButtonInGame.GetComponent<ShowCardInformation>();


            if (clickCount == 0)
            {
                checkForUpgradeActivity.upgradeStationActive = true;

                if (checkForUpgradeActivity.cardID == 1)
                {
                    checkForUpgradeActivity.cardOneInfo.SetActive(false);
                }

                if (checkForUpgradeActivity.cardID == 2)
                {
                    checkForUpgradeActivity.cardTwoInfo.SetActive(false);
                }

                FadeInTranOneUpgrades();

                clickCount = 2;
            }

            if (clickCount == 1)
            {
                checkForUpgradeActivity.upgradeStationActive = false;

                FadeOutTranOneUpgrades();

                clickCount = 1;
            }
        }
    }

    public void ManufacturingButtonClicked()
    {
        if(inAction == false)
        {
            clickCount = clickCount - 1;

            GameObject manButtonInGame = GameObject.Find("Manufacturing Button");

            ShowCardInformation checkForUpgradeActivity = manButtonInGame.GetComponent<ShowCardInformation>();


            if (clickCount == 0)
            {
                checkForUpgradeActivity.upgradeStationActive = true;

                if (checkForUpgradeActivity.cardID == 1)
                {
                    checkForUpgradeActivity.cardOneInfo.SetActive(false);
                }

                if (checkForUpgradeActivity.cardID == 2)
                {
                    checkForUpgradeActivity.cardTwoInfo.SetActive(false);
                }

                FadeInManUpgrades();

                clickCount = 2;
            }

            if (clickCount == 1)
            {
                checkForUpgradeActivity.upgradeStationActive = false;

                FadeOutManUpgrades();

                clickCount = 1;
            }
        }    
    }

    public void TransportToEuropeButtonClicked()
    {
        if(inAction == false)
        {
            clickCount = clickCount - 1;

            GameObject tranTwoButtonInGame = GameObject.Find("Transportation To Europe Button");

            ShowCardInformation checkForUpgradeActivity = tranTwoButtonInGame.GetComponent<ShowCardInformation>();


            if (clickCount == 0)
            {
                checkForUpgradeActivity.upgradeStationActive = true;

                if (checkForUpgradeActivity.cardID == 1)
                {
                    checkForUpgradeActivity.cardOneInfo.SetActive(false);
                }

                if (checkForUpgradeActivity.cardID == 2)
                {
                    checkForUpgradeActivity.cardTwoInfo.SetActive(false);
                }

                FadeInTranTwoUpgrades();

                clickCount = 2;
            }

            if (clickCount == 1)
            {
                checkForUpgradeActivity.upgradeStationActive = false;

                FadeOutTranTwoUpgrades();

                clickCount = 1;
            }
        }
    }

    public void EuropeDistributionButtonClicked()
    {
        if(inAction == false)
        {
            clickCount = clickCount - 1;

            GameObject europeDisButtonInGame = GameObject.Find("Europe Distribution Button");

            ShowCardInformation checkForUpgradeActivity = europeDisButtonInGame.GetComponent<ShowCardInformation>();


            if (clickCount == 0)
            {
                checkForUpgradeActivity.upgradeStationActive = true;

                if (checkForUpgradeActivity.cardID == 1)
                {
                    checkForUpgradeActivity.cardOneInfo.SetActive(false);
                }

                if (checkForUpgradeActivity.cardID == 2)
                {
                    checkForUpgradeActivity.cardTwoInfo.SetActive(false);
                }

                FadeInEuropeDisUpgrades();

                clickCount = 2;
            }

            if (clickCount == 1)
            {
                checkForUpgradeActivity.upgradeStationActive = false;

                FadeOutEuropeDisUpgrades();

                clickCount = 1;
            }
        }      
    }
}
