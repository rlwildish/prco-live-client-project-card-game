﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



    public class PlayerResources : MonoBehaviour
    {

        // Script Made By: Sami Zirak

        public int cotwoLevels;            //The amount of CO2 emissions the player is responislbe for. 

        public double moneyLevels;            //The amount of money the player has.    

        public double expectedIncome;

        public double costToBuild;

        public double maintenanceCost;

        public int ethicsLevels;           //The players ethical level.

        public int playerTurn = 1;         //The player starts the game on turn one.

        public Slider cotwoLevelSlider;     //The UI Slider that displays the players CO2 Level.

        public Text moneyLevelText;         //Text that diplays the amount of money the player has.

        public Slider ethicsLevelSlider;    //The UI Slider that displays the players Ethics Level.

        public Text currentTurnText;        //Text that tells the player what turn it is.

        public Text expectedIncomeText;

        private bool harvestingOptionOne = true;
        private bool harvestingOptionTwo = false;

        private bool transportToManuOptionOne = true;
        private bool transportToManuOptionTwo = false;

        private bool manufacturerOptionOne = true;
        private bool manufacturerOptionTwo = false;

        private bool transportToDistOptionOne = true;
        private bool transportToDistOptionTwo = false;

        private bool distributionOptionOne = true;
        private bool distributionOptionTwo = false;

        public double harvestingMoney;

        public double transToManuMoney;

        public double manufacturerMoney;

        public double transToEuropeMoney;

        public double distributionMoney;

        public double revenue;


        void Start()
        {
            cotwoLevels = 0;        //Players CO2 emmisions start out at 0.
            moneyLevels = 1000000;    //Players are given 1 Million at the start of the game.
            ethicsLevels = 0;       //Players havent got an ethical level at the start of the game.
            expectedIncome = 0;       
        }


        void Update()
        {

            cotwoLevelSlider.value = cotwoLevels;                               //Make the CO2 Slider display the level of C02 emissions the player has built up.
            moneyLevelText.text = "£" + moneyLevels.ToString();                 //Makes the UI Text display the amount of money the player has.
            ethicsLevelSlider.value = ethicsLevels;                             //Makes the Ethics Slider display the Ethical level of the player.
            currentTurnText.text = "Current Turn: " + playerTurn.ToString();    //Displays the turn the player is on above the next turn button.
            expectedIncomeText.text = "£" + expectedIncome.ToString();
        }

        public void NextTurnButton()
        {
            expectedIncome = 0;
            playerTurn = playerTurn + 1;    //Whenever the next turn button is pressed this method is called and it advances the game to the next turn.
            Harvesting();
            TransportToManufacturer();
            Manufacturer();
            TransportToDistribution();
            Distribution();
            StartCoroutine(MyCoroutine());
            
        }

        IEnumerator MyCoroutine()
        {

            yield return 0.5f;

            CalculateRevenue();
            harvestingMoney = 0;
            manufacturerMoney = 0;
            transToManuMoney = 0;
            transToEuropeMoney = 0;
            distributionMoney = 0;
            revenue = 0;

        }

    public void CardDelayAndDurationReduction()
    {

        GameObject findEndOfTurnCards = GameObject.Find("Main Camera");

        EndOfTurnCards findEndTurnCards = findEndOfTurnCards.GetComponent<EndOfTurnCards>();

        

        findEndTurnCards.card0Delay--;
        findEndTurnCards.card0Duration--;
        findEndTurnCards.card1Delay--;
        findEndTurnCards.card1Duration--;
        findEndTurnCards.card2Delay--;
        findEndTurnCards.card2Duration--;
        findEndTurnCards.card3Delay--;
        findEndTurnCards.card3Duration--;
        findEndTurnCards.card4Delay--;
        findEndTurnCards.card4Duration--;
        findEndTurnCards.card5Delay--;
        findEndTurnCards.card5Duration--;
        findEndTurnCards.card6Delay--;
        findEndTurnCards.card6Duration--;
        findEndTurnCards.card7Delay--;
        findEndTurnCards.card7Duration--;
        findEndTurnCards.card8Delay--;
        findEndTurnCards.card8Duration--;
        findEndTurnCards.card9Delay--;
        findEndTurnCards.card9Duration--;
        findEndTurnCards.card10Delay--;
        findEndTurnCards.card10Duration--;
        findEndTurnCards.card11Delay--;
        findEndTurnCards.card11Duration--;
        findEndTurnCards.card12Delay--;
        findEndTurnCards.card12Duration--;
        findEndTurnCards.card13Delay--;
        findEndTurnCards.card13Duration--;
        findEndTurnCards.card14Delay--;
        findEndTurnCards.card14Duration--;
        findEndTurnCards.card15Delay--;
        findEndTurnCards.card15Duration--;

}

        public void Harvesting()
        {
            if (harvestingOptionOne == true)
            {
                cotwoLevels += 10;
                harvestingMoney -= 10000;
                expectedIncome -= 10000;
                ethicsLevels -= 2;
            }
            else if (harvestingOptionTwo == true)
            {
                cotwoLevels += 10;
                harvestingMoney -= 15000;
                expectedIncome -= 15000;
            }
        }

        public void SelectHarvestingOne()
        {
            if (harvestingOptionOne == false && moneyLevels >= 50000)
            {
                harvestingOptionOne = true;
                harvestingOptionTwo = false;
                moneyLevels -= 50000;
            }
        }

        public void SelectHarvestingTwo()
        {
            if (harvestingOptionTwo == false && moneyLevels >= 150000)
            {
                harvestingOptionOne = false;
                harvestingOptionTwo = true;
                moneyLevels -= 150000;
            }
        }

        public void TransportToManufacturer()
        {
            if (transportToManuOptionOne == true)
            {
                cotwoLevels += 5;
                transToManuMoney -= 2500;
                expectedIncome -= 2500;
            }
            else if (transportToManuOptionTwo == true)
            {
                cotwoLevels += 10;
                transToManuMoney -= 5000;
                expectedIncome -= 5000;
            }
        }

        public void SelectTransportToManuOne()
        {
            if (transportToManuOptionOne == false && moneyLevels >= 5000)
            {
                transportToManuOptionOne = true;
                transportToManuOptionTwo = false;
                moneyLevels -= 5000;
            }
        }

        public void SelectTransportToManuTwo()
        {
            if (transportToManuOptionTwo == false && moneyLevels >= 15000)
            {
                transportToManuOptionOne = false;
                transportToManuOptionTwo = true;
                moneyLevels -= 15000;
            }
        }

        public void Manufacturer()
        {
            if (manufacturerOptionOne == true)
            {
                cotwoLevels += 10;
                manufacturerMoney -= 10000;
                ethicsLevels -= 3;
                expectedIncome -= 10000;
            }
            else if (manufacturerOptionTwo == true)
            {
                cotwoLevels += 15;
                manufacturerMoney -= 15000;
                expectedIncome -= 15000;    
            }
        }

        public void SelectManufacturerOne()
        {
            if (manufacturerOptionOne == false && moneyLevels >= 50000)
            {
                manufacturerOptionOne = true;
                manufacturerOptionTwo = false;
                moneyLevels -= 50000;
            }
        }

        public void SelectManufacturerTwo()
        {
            if (manufacturerOptionTwo == false && moneyLevels >= 200000)
            {
                manufacturerOptionOne = false;
                manufacturerOptionTwo = true;
                moneyLevels -= 200000;
            }
        }

        public void TransportToDistribution()
        {
            if (transportToDistOptionOne == true)
            {
                cotwoLevels += 0;
                transToEuropeMoney -= 2500;
                expectedIncome -= 2500;
            }
            else if (transportToDistOptionTwo == true)
            {
                cotwoLevels += 10;
                transToEuropeMoney -= 5000;
                expectedIncome -= 5000;
            }
        }

        public void SelectTransportToDistOne()
        {
            if (transportToDistOptionOne == false && moneyLevels >= 5000)
            {
                transportToDistOptionOne = true;
                transportToDistOptionTwo = false;
                moneyLevels -= 5000;
            }
        }

        public void SelectTransportToDistTwo()
        {
            if (transportToDistOptionTwo == false && moneyLevels >= 15000)
            {
                transportToDistOptionOne = false;
                transportToDistOptionTwo = true;
                moneyLevels -= 15000;
            }
        }

        public void Distribution()
        {
            if (distributionOptionOne == true)
            {
                cotwoLevels += 10;
                distributionMoney -= 10000;
                expectedIncome -= 10000;
            }
            else if (distributionOptionTwo == true)
            {
                cotwoLevels += 15;
                distributionMoney -= 7500;
                expectedIncome -= 7500;
            }
        }

        public void SelectDistributionOne()
        {
            if (distributionOptionOne == false && moneyLevels >= 5000)
            {
                distributionOptionOne = true;
                distributionOptionTwo = false;
                moneyLevels -= 5000;
            }
        }

        public void SelectDistributionTwo()
        {
            if (distributionOptionTwo == false && moneyLevels >= 15000)
            {
                distributionOptionOne = false;
                distributionOptionTwo = true;
                moneyLevels -= 15000;
            }
        }

        public void CalculateRevenue()
        {
            revenue = (harvestingMoney + transToManuMoney + manufacturerMoney + transToEuropeMoney + distributionMoney);

            moneyLevels += revenue;
        }

        public void SetCOTwoLevels(int newValue)
        {

            cotwoLevels = newValue;

        }

        public void AlterCOTwoLevels(int newValue)
        {
            cotwoLevels += newValue;
        }

        public void SetEthicsLevels(int newValue)
        {

            ethicsLevels = newValue;

        }

        public void AlterEthicsLevels(int newValue)
        {
            ethicsLevels += newValue;
        }

        public void SetMoneyLevels(double newValue)
        {

            moneyLevels = newValue;

        }

        public void SetExpectedIncome(double newValue)
        {
            expectedIncome = newValue;
        }

        public void AlterExpectedIncome(double newValue)
        {
            expectedIncome += newValue;
        }

        public int GetCOTwoLevels()
        {
            return cotwoLevels;
        }

        public int GetEthicsLevels()
        {
            return ethicsLevels;
        }

        public double GetMoneyLevels()
        {
            return moneyLevels;
        }

        public double GetExpectedIncome()
        {
            return expectedIncome;
        }

        public void SetHarvestingMoney(double newValue)
        {
            harvestingMoney += newValue;
        }

        public void SetTransToManuMoney(double newValue)
        {
            transToManuMoney += newValue;
        }

        public void SetManufacturerMoney(double newValue)
        {
            manufacturerMoney += newValue;
        }

        public void SetTransToDistMoney(double newValue)
        {
            transToEuropeMoney += newValue;
        }

        public void SetDistMoney(double newValue)
        {
            distributionMoney += newValue;
        }

    }
