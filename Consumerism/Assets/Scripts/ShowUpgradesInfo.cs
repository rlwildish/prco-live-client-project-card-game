﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUpgradesInfo : MonoBehaviour {

    public GameObject cardInformation;

	// Use this for initialization
	void Start ()
    {
        cardInformation.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void onMouseOver()
    {
        cardInformation.SetActive(true);
    }

    public void onMouseExit()
    {
        cardInformation.SetActive(false);
    }

}
